<?php 
$rootDir = "/home/cloudpanel/htdocs/www.itshot.com/current/";
$backupDirectory = "upload_images_without_replace/old/";
$adempierDirectory = "adempierstock/imagescript/adempier-images-update/";
$filePostFixAdempier = "update-image-path.txt";
$logFileAdempier = $rootDir . $adempierDirectory . $filePostFixAdempier;
$filePostFix = "_" . date("d-m-Y_H-i-s") . ".txt";
$logFile = $rootDir . "adempierstock/imagescript/log/log_override-images" . $filePostFix;
$logFileWebpath = "https://www.itshot.com/adempierstock/imagescript/log/log_override-images" . $filePostFix;
$fileError = "update-image-error.txt";
$logFileError = $rootDir . $adempierDirectory . $fileError;
$fileImagesUpdate = "fileImagesUpdate". date("d-m-Y_H-i-s") . ".txt";
$logFileImagesUpdate =$rootDir . $adempierDirectory . $fileImagesUpdate;
