<?php

include 'functions_withoutreplace_img.php';

$respones = array(
    'errno' => 0,
    'msg' => 'Success'
);
$filename = isset($_GET['filename']) ? $_GET['filename'] : '';
if ($filename) {
    $imageInfo = parseImage($filename);
    if ($imageInfo) {
        list($sku, $appendStr, $fileExtension) = $imageInfo;
        $getEntityId = "SELECT entity_id FROM `" . $tablePrefix . "catalog_product_entity` WHERE `sku` = '" . $sku . "'";
        $entityIdResult = $conn->fetchCol($getEntityId);
        $entityId = $entityIdResult[0];
        if ($entityId) {
            $imageNameSql = "SELECT * FROM `" . $tablePrefix . "catalog_product_entity_media_gallery` AS mgallery, " . $tablePrefix . "catalog_product_entity_media_gallery_value AS mgalleryvalue WHERE entity_id =  '" . $entityId . "' AND mgallery.value_id = mgalleryvalue.value_id  AND mgalleryvalue.disabled = 0 AND store_id = 0";
            $imageNameResult = $conn->fetchAll($imageNameSql);
            $productId = $entityId;

            $urlKey = selectProductUrlKey($productId);
            if ($urlKey == '') {
                $urlSQL = "SELECT value FROM " . $tablePrefix . "catalog_product_entity_varchar WHERE attribute_id=97 AND entity_id=" . $productId;
                $urlSQLRes = $conn->fetchRow($urlSQL);
                $urlKey = $urlSQLRes["value"];
            }
            $lab = $appendStr;
            $ismain = 0;
            if (strstr($appendStr, "main")) {
                $appendStr = str_replace("main", "", $appendStr);
                $ismain = 1;
            }

            if ($appendStr == '') {
                $appendStr = 'main';
            }
            if ($appendStr == '2') {
                $appendStr = 'main2';
            }
            if ($appendStr == '3') {
                $appendStr = 'main3';
            }
            $newImgName = $urlKey . "_" . $appendStr . "." . $fileExtension;
            $label = $appendStr;
            $oldlabel = "old ".$label;
            $position = array_search($lab, $labels) + 1;
            $positionoldImages =$position + 1;
            $urlKeyWithType = $urlKey . $appendStr;
            $ademiperUrlKey = $urlKey . "_" . $position;

            $p_imgarray = array('main' => 0, 'back' => 0, 'bod' => 0, 'box' => 0);
            $issuewithimage = 0;
            $oldimage = getoldimage($appendStr, $productId);
            $imgname = basename($oldimage);
            $imgpath = str_replace($imgname, '', $oldimage);
            $imgExistingPath = getExistingImagePath($productId, $imgname);

            if (file_exists($rootDir . "media/catalog/product" . $oldimage)) {
                $checkOldImage = array();
                $renameOldImage = array();
                $checkOldImage = explode(".", $oldimage);
                if(count($checkOldImage)==2){
                    $newImgName = $checkOldImage[0]."-".date("Ymd").".".$checkOldImage[1];  
                    $renameOldImage = $checkOldImage[0].'___'.date("Ymd-H-s").".".$checkOldImage[1];    
                    rename($rootDir . "media/catalog/product" . $oldimage,$rootDir . "media/catalog/product" . $renameOldImage);
                }elseif(count($checkOldImage)==3){
                    $newImgName = $checkOldImage[0].$checkOldImage[1]."-".date("Ymd").".".$checkOldImage[2];
                    $renameOldImage = $checkOldImage[0].$checkOldImage[1].'___'.date("Ymd-H-s").".".$checkOldImage[2];    
                    rename($rootDir . "media/catalog/product" . $oldimage,$rootDir . "media/catalog/product" . $renameOldImage);
                }else{
                    echo 'Please format the image accordingly';
                    exit;
                }

                if (copy($rootDir . "adempierstock/imagescript/upload_images_without_replace/" . $filename, $rootDir . "media/catalog/product" . $newImgName)) {
                    
                    if (file_exists($rootDir . "media/catalog/product" . $newImgName) && file_exists($rootDir . "media/catalog/product" . $renameOldImage)) {
                        updatenewimagelabel_position($productId, $label, $position, $oldimage, $sku, $ismain, $filename,$newImgName);
                        $imgnewname = basename($newImgName); //get name of images
                        $imgExistingPath = getExistingImagePath($productId, $imgnewname);
                        resize($imgnewname, $imgExistingPath);
                        copyInAnotherFolder($filename); // copy file upload images  to folder old

                        //update data for old images
                        updateimagelabel_position($productId, $oldlabel, $positionoldImages, $renameOldImage, $sku, 0, $filename);
                        $imgoldname = basename($renameOldImage);
                        $imgExistingPath = getExistingImagePath($productId, $imgoldname);
                        resize($imgoldname, $imgExistingPath);

                        //reindex special product
                        $product = Mage::getModel('catalog/product')->load($productId); // Product Id
                        $event = Mage::getSingleton('index/indexer')->logEvent(
                                $product,
                                $product->getResource()->getType(),
                                Mage_Index_Model_Event::TYPE_SAVE,
                                false
                            );
                            Mage::getSingleton('index/indexer')
                                ->getProcessByCode('catalog_product_flat') // Adjust the indexer process code as needed
                                ->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)
                                ->processEvent($event);
                        
                    }else{
                        if (!file_exists($logFileError)) {
                            $currentFile = fopen($logFileError, "w");
                            fclose($currentFile);
                        }
                        $stringData = $filename ."-".$newImgName." file not exists or ".$renameOldImage." file not exists or". "\n";
                        file_put_contents($logFileError, $stringData, FILE_APPEND);
                    }
                   
                }else{
                        if (!file_exists($logFileError)) {
                            $currentFile = fopen($logFileError, "w");
                            fclose($currentFile);
                        }
                        $stringData = $filename ."--".$newImgName." can not copy". "\n";
                        file_put_contents($logFileError, $stringData, FILE_APPEND);
                    }
            } else {

                $sourceImage = $rootDir . "adempierstock/imagescript/upload_images_without_replace/".$filename;
                $targetImage = $rootDir . "media/catalog/product/{$imgExistingPath}/".$newImgName;
                $targetDir = dirname($targetImage);
                if (!is_dir($targetDir)) {
                    mkdir($targetDir);
                }

                if (copy($sourceImage,  $targetImage)) {
                    if (file_exists($targetImage)) {
                        updateimagelabel_position($productId, $label, $position, "/{$imgExistingPath}/" . $newImgName, $sku, $ismain, $filename);
                        resize($newImgName, $imgExistingPath);
                        copyInAnotherFolder($filename);


                        //reindex special product
                        $product = Mage::getModel('catalog/product')->load($productId); // Product Id
                        $event = Mage::getSingleton('index/indexer')->logEvent(
                                $product,
                                $product->getResource()->getType(),
                                Mage_Index_Model_Event::TYPE_SAVE,
                                false
                            );
                            Mage::getSingleton('index/indexer')
                                ->getProcessByCode('catalog_product_flat') // Adjust the indexer process code as needed
                                ->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)
                                ->processEvent($event);

                    }else{
                        if (!file_exists($logFileError)) {
                            $currentFile = fopen($logFileError, "w");
                            fclose($currentFile);
                        }
                        $stringData = $filename ."---".$newImgName." file not exist". "\n";
                        file_put_contents($logFileError, $stringData, FILE_APPEND);
                    }
                    
                }else{
                        if (!file_exists($logFileError)) {
                            $currentFile = fopen($logFileError, "w");
                            fclose($currentFile);
                        }
                        $stringData = $filename ."----".$newImgName." can not copy". "\n";
                        file_put_contents($logFileError, $stringData, FILE_APPEND);
                    }
            }
        }
    }
}

header('Content-Type: application/json');
echo json_encode($respones);

